import type { Branch } from './Branch'
import type { Member } from './Member'
import type { Promotion } from './Promotion'
import type { ReceiptItem } from './ReceiptItem'
import type { User } from './User'

type PaymentType = 'promtpay' | 'cash'
type Receipt = {
  id?: number
  createdDate: Date
  totalPrice: number
  discount: number
  income: number
  qty: number
  change: number
  paymentType: PaymentType
  userId: number
  memberId: number
  branchId: number
  promotionId: number
  receiptDetails?: ReceiptItem[]
  user?: User
  member?: Member
  branch?: Branch
  promotion?: Promotion
  promotionDiscount: number
}

export type { Receipt, PaymentType }
