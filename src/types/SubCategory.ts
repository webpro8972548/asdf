type SubCategory = {
  id?: number
  name: string
}

export type { SubCategory }
