import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        default: () => import('../views/HomeView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/pos',
      name: 'pos',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/POS/PosView.vue'),
        paymentView: () => import('../views/POS/PaymentView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/pos/paymentView',
      name: 'payment',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/POS/PaymentView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/user',
      name: 'user',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/User/UserView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/product',
      name: 'product',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/Product/ProductView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/member',
      name: 'member',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/Member/MemberView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/promotion',
      name: 'promotion',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/Promotion/MainPromotion.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/LoginMember.vue'),
      meta: {
        layout: 'FullLayout',
        requireAuth: false
      }
    },
    {
      path: '/promember',
      name: 'promember',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/Member/PromemberView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/infomember',
      name: 'infomember',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../views/Member/InfomemberView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    }
    
  ]
})


export default router
