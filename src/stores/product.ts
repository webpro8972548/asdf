import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import { useLoadingStore } from './loading'
import productService from '@/services/product'
import { nextTick, ref, type Ref } from 'vue'
import type { VForm } from 'vuetify/components'

export const useProductStore = defineStore('product', () => {
  const dialog = ref(false)
  const deleteDialog = ref(false)
  const form = ref(false)
  const loading = ref(false)
  const refForm = ref<VForm | null>(null)
  const categoryFil: Ref<string> = ref('all')
  // let lastId = 2

  const initiProduct: Product & { files: File[] } = {
    name: '',
    image: 'noimage.PNG',
    price: 0,
    unit: '',
    category: { id: 1, name: 'เครื่องดื่ม' },
    subCategorys: [],
    sizes: [],
    files: []
  }

  const editedProduct = ref<Product & { files: File[] }>(JSON.parse(JSON.stringify(initiProduct)))
  let editedIndex = -1
  const loadingStore = useLoadingStore()
  const products = ref<Product[]>([])

  function closeDelete() {
    deleteDialog.value = false
    nextTick(() => {
      clearForm()
    })
  }
  async function deleteItemConfirm() {
    // Delete item from list
    await deleteProduct()
    closeDelete()
  }
  async function editItem(item: Product) {
    if (!item.id) return
    await getProduct(item.id)
    dialog.value = true
    editedIndex = -1
  }
  async function deleteItem(item: Product) {
    if (!item.id) return
    await getProduct(item.id)
    deleteDialog.value = true
    editedIndex = -1
  }
  function closeDialog() {
    dialog.value = false
    nextTick(() => {
      clearForm()
      editedIndex = -1
    })
  }

  function onSubmit() {}

  async function getProduct(id: number) {
    try {
      loadingStore.doLoad()
      const res = await productService.getProduct(id)
      editedProduct.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      console.log(e.message)
    }
  }
  async function getProducts() {
    try {
      loadingStore.doLoad()
      const res = await productService.getProducts()
      products.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }
  async function saveProduct() {
    // loadingStore.doLoad()
    const product = editedProduct.value
    if (!product.id) {
      console.log('Post' + JSON.stringify(product))
      const res = await productService.addProduct(product)
      console.log('Posted' + JSON.stringify(product))
    } else {
      console.log('Patch' + JSON.stringify(product))
      const res = await productService.updateProduct(product)
    }
    clearForm()
    await getProducts()
    // loadingStore.finish()
  }
  async function deleteProduct() {
    loadingStore.doLoad()
    const product = editedProduct.value
    const res = await productService.delProduct(product)
    clearForm()
    await getProducts()
    loadingStore.finish()
  }

  function clearForm() {
    editedProduct.value = JSON.parse(JSON.stringify(initiProduct))
  }
  function checkDrink() {
    if (editedProduct.value.category.name === 'เครื่องดื่ม') {
      return true
    }
    editedProduct.value.subCategorys = []
    editedProduct.value.sizes = []
    return false
  }

  return {
    products,
    editedProduct,
    dialog,
    form,
    deleteDialog,
    categoryFil,
    close,
    clearForm,
    onSubmit,
    closeDelete,
    deleteItemConfirm,
    editItem,
    deleteItem,
    closeDialog,
    getProducts,
    getProduct,
    saveProduct,
    deleteProduct,
    checkDrink
  }
})
