import { useLoadingStore } from './loading'
import { nextTick, ref, type Ref } from 'vue'
import { defineStore } from 'pinia'
import userService from '@/services/user'
import type { User } from '@/types/User'

export const useUserStore = defineStore('user', () => {
  const form = ref(false)
  const dialog = ref(false)
  const dialogDelete = ref(false)
  const loading = ref(false)
  const passwordDialog = ref(false)
  const profileDialog = ref(false)
  const newUser = ref(false)
  const filterDialog = ref(false)

  const loadingStore = useLoadingStore()
  const users = ref<User[]>([])
  const initialUser: User & { files: File[] } = {
    name: 'นพาพร บุญพรรณ',
    gender: '',
    height: 0,
    weight: 0,
    bloodType: '',
    age: 0,
    birthDate: new Date(),
    phone: '084-123-7890',
    email: 'noppa@dc.th',
    address: '180 หมู่ 2 ต.บางแสน อ.บางแสน จ.ชลบุรี 20131',
    role: {
      id: 1,
      name: 'เจ้าของร้าน'
    },
    startDate: new Date(),
    status: 'ยังอยู่',
    salary: 250000,
    branch: {
      name: '',
      address: '180 หมู่ 2 ต.บางแสน',
      city: 'บางแสน',
      province: 'ชลบุรี',
      country: 'ไทย',
      postalcode: '20131',
      latitude: 13.281264959275443,
      longitude: 100.92410270978652
    },
    password: 'Pass@1234',
    image: 'noimage.jpg',
    files: []
  }

  const editedUser = ref<User & { files: File[] }>(JSON.parse(JSON.stringify(initialUser)))
  async function getUser(id: number) {
    loadingStore.doLoad()
    try {
      const res = await userService.getUser(id)
      editedUser.value = res.data
    } catch (error) {
      console.error('Error fetching user:', error)
    } finally {
      loadingStore.finish()
    }
  }

  async function getUsers() {
    loadingStore.doLoad()
    try {
      const res = await userService.getUsers()
      users.value = res.data
    } catch (error) {
      console.error('Error fetching users:', error)
    } finally {
      loadingStore.finish()
    }
  }

  async function saveUser() {
    loadingStore.doLoad()
    try {
      const user = editedUser.value
      let res
      if (!user.id) {
        console.log('Post ' + JSON.stringify(user))
        res = await userService.addUser(user)
      } else {
        console.log('Patch ' + JSON.stringify(user))
        res = await userService.updateUser(user)
      }
      if (res) {
        await getUsers()
      }
    } catch (error) {
      console.error('Error saving user:', error)
    } finally {
      loadingStore.finish()
    }
  }

  async function deleteUser() {
    loadingStore.doLoad()
    try {
      const user = editedUser.value
      await userService.delUser(user)
      await getUsers()
    } catch (error) {
      console.error('Error deleting user:', error)
    } finally {
      loadingStore.finish()
    }
  }

  function clearForm() {
    editedUser.value = JSON.parse(JSON.stringify(initialUser))
  }

  async function changePassword() {
    const user = editedUser.value
    await userService.changePassword(user)
    clearForm()
    passwordDialog.value = false
  }

  function onSubmit() { }

  function closeDelete() {
    dialogDelete.value = false
    nextTick(() => {
      clearForm()
    })
  }
  async function profileOpen(item: User) {
    if (!item.id) return
    await getUser(item.id)
    profileDialog.value = true
  }

  async function editItem(item: User) {
    if (!item.id) return
    await getUser(item.id)
    dialog.value = true
  }

  async function deleteItem(item: User) {
    if (!item.id) return
    await getUser(item.id)
    dialogDelete.value = true
  }

  function closeDialog() {
    dialog.value = false
    passwordDialog.value = false
    nextTick(() => {
      if (profileDialog.value) return
      clearForm()
    })
  }
  close

  async function changePasswordDialog() {
    await changePassword()
    passwordDialog.value = false
  }

  async function deleteItemConfirm() {
    await deleteUser()
    closeDelete()
  }
  function getCurrentUser(): User | null {
    const strUser = localStorage.getItem('user')
    if (strUser === null) return null
    const user = JSON.parse(strUser)
    // Ensure the user object has the image property
    if (!user || !user.image) return null
    return user
  }

  function closeProfile() {
    profileDialog.value = false
    nextTick(() => {
      clearForm()
    })
  }

  //Filter
  const genderFil = ref('all')
  const minAgeFil = ref(0)
  const maxAgeFil = ref(100)
  const roleFil = ref('all')
  const statusFil = ref('all')
  const branchFil = ref('all')
  const minSalaryFil = ref(0)
  const maxSalaryFil = ref(100000)

  async function getByFilter() {
    const fil: Ref<any[]> = ref([])
    const afterFil: Ref<any[]> = ref([])
    try {
      loadingStore.doLoad()
      const filterData = await userService.getByFilter(
        genderFil.value,
        roleFil.value,
        branchFil.value,
        statusFil.value,
        minSalaryFil.value,
        maxSalaryFil.value,
        minAgeFil.value,
        maxAgeFil.value
      )
      console.log('filterData: ', filterData)
      fil.value = filterData.data
      const userIds = fil.value.map((item: any) => item.EMP_ID)
      const userPromises = userIds.map((userId) => userService.getUser(userId))
      const userResponses = await Promise.all(userPromises)
      afterFil.value = userResponses.filter((res) => res.data).map((res) => res.data)
      users.value = afterFil.value
    } catch (error) {
      console.error('Error filtering users:', error)
    } finally {
      loadingStore.finish()
      filterDialog.value = false
    }
  }

  async function CalculateSalary(id: number) {
    try {
      loadingStore.doLoad()
      const res = await userService.CalculateSalary(id)
      loadingStore.finish()
      return res.data
    } catch (e: any) {
      loadingStore.finish()
    }
  }
  return {
    // variables
    users,
    editedUser,
    form,
    dialog,
    dialogDelete,
    loading,
    passwordDialog,
    profileDialog,
    newUser,
    genderFil,
    minAgeFil,
    maxAgeFil,
    roleFil,
    statusFil,
    branchFil,
    minSalaryFil,
    maxSalaryFil,
    filterDialog,
    // methods
    getUsers,
    saveUser,
    deleteUser,
    getUser,
    clearForm,
    changePassword,
    closeDialog,
    closeDelete,
    editItem,
    deleteItem,
    deleteItemConfirm,
    onSubmit,
    changePasswordDialog,
    profileOpen,
    closeProfile,
    getCurrentUser,
    CalculateSalary,
    getByFilter
  }
})
