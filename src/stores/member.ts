import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import memberService from '@/services/member'
import type { Member } from '@/types/Member'
//import { useReceiptStore } from './receipt'
import router from '@/router'

export const useMemberStore = defineStore('member', () => {
  const loadingStore = useLoadingStore()
  const members = ref<Member[]>([])
  //const receiptStore = useReceiptStore()
  const initilMember: Member = {
    fristName: '',
    lastName: '',
    email: '',
    password:'',
    tel: '',
    birth: new Date(),
    point: 0,
    pointRate: 0,
    indate: new Date()
  }
  const editedMember = ref<Member>(JSON.parse(JSON.stringify(initilMember)))

  const currentMember = ref<Member | null>(null)
  async function searchMember(tel: string) {
    loadingStore.doLoad()
    const res = await memberService.getMembers()
    members.value = res.data
    loadingStore.finish()

    const index = members.value.findIndex((item) => item.tel === tel)
    if (index < 0) {
      currentMember.value = null
      clearFrom()
    } else {
      currentMember.value = members.value[index]
      //receiptStore.receipt!.member = JSON.parse(JSON.stringify(currentMember.value))
    }
  }

  function getCurrentMember(): Member | null {
    return currentMember.value
  }

  async function getMember(id: number) {
    loadingStore.doLoad()
    const res = await memberService.getMember(id)
    editedMember.value = res.data
    loadingStore.finish()
  }

  async function getMembers() {
    loadingStore.doLoad()
    const res = await memberService.getMembers()
    members.value = res.data
    loadingStore.finish()
  }

  async function saveMember() {
    loadingStore.doLoad()
    const member = editedMember.value
    if (!member.id) {
      // Add new
      console.log('Post ' + JSON.stringify(member))
      const res = await memberService.addMember(member)
    } else {
      //update
      console.log('Patch ' + JSON.stringify(member))
      const res = await memberService.updateMember(member)
    }
    clearFrom()
    await getMembers()
    loadingStore.finish()
  }

  async function deleteMember() {
    loadingStore.doLoad()
    const member = editedMember.value
    const res = await memberService.delMember(member)
    clearFrom()
    await getMembers()
    loadingStore.finish()
  }

  function clearFrom() {
    editedMember.value = JSON.parse(JSON.stringify(initilMember))
  }

  function clear() {
    return {
      fristName: '',
      lastName: '',
      email: '',
      tel: '',
      birth: new Date(),
      point: 0,
      pointRate: 0,
      indate: new Date()
    }
  }
  async function loginMember(email: string, password : string) {
    loadingStore.doLoad()
    const res = await memberService.getMembers()
    members.value = res.data
    loadingStore.finish()

    const index = members.value.findIndex((item) => item.email === email && item.password === password)
    if (index < 0) {
      currentMember.value = null
      clearFrom()
    } else {
      currentMember.value = members.value[index]
    }
  }

  const logout = function () {
    console.log('Logout')
    router.replace('/login')
  }

  return {
    members,
    currentMember,
    getCurrentMember,
    searchMember,
    getMember,
    getMembers,
    saveMember,
    deleteMember,
    editedMember,
    clearFrom,
    clear,
    loginMember,
    logout
  }
})
