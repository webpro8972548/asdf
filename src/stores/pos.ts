import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import { useLoadingStore } from './loading'
import productService from '@/services/product'

export const usePosStore = defineStore('pos', () => {
  const loadingStore = useLoadingStore()

  const drinkProducts = ref<Product[]>([])
  const bakeryProducts = ref<Product[]>([])
  const foodProducts = ref<Product[]>([])

  async function getProducts() {
    try {
      loadingStore.doLoad()
      let res = await productService.getProductsByCategory(1)
      drinkProducts.value = res.data
      res = await productService.getProductsByCategory(2)
      foodProducts.value = res.data
      res = await productService.getProductsByCategory(3)
      bakeryProducts.value = res.data
      loadingStore.finish()
    } catch (e) {
      console.log('error')
      loadingStore.finish()
    }
  }

  return { drinkProducts, bakeryProducts, foodProducts, getProducts }
})
