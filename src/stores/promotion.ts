import type { Promotion } from '@/types/Promotion'
import { nextTick, ref } from 'vue'
//import { useReceiptStore } from '@/stores/receipt' // ต้องการ import useReceiptStore
import { useLoadingStore } from './loading'
import promotionService from '@/services/promotion'
import { format } from 'date-fns'

export function usePromotionStore() {
  const promotions = ref<Promotion[]>([])
  const dialog = ref(false)
  const dialogDelete = ref(false)
  const from = ref(false)

  const initialPromotion: Promotion = {
    name: '',
    discount: 0,
    startDate: new Date(),
    endDate: new Date(),
    details: 'Member',
    pointUse: 0,
    category: { id: 1, name: 'เครื่องดื่ม' },
    status: 'Active'
  }

  const editedPromotion = ref<Promotion>(JSON.parse(JSON.stringify(initialPromotion)))
  let editedIndex = -1
  // let lastId = 4

  const loadindStore = useLoadingStore()
  const loading = ref(false)

  async function getPromotions() {
    loadindStore.doLoad()
    const res = await promotionService.getPromotions()
    promotions.value = res.data
    loadindStore.finish()
  }

  // function initialize() {
  //   promotions.value = [
  //     {
  //       id: 1,
  //       proName: 'โปรวันเกิดลด 10 %',
  //       discount: 10,
  //       startPro: '01-01-2567',
  //       endPro: '31-12-2568',
  //       detail: 'ส่วนลดเพื่อ10% ในวัดเกิดของลูกค้า'
  //     },
  //     {
  //       id: 2,
  //       proName: 'โปรโมชั่นวันปีใหม่ลด 10 บาท',
  //       discount: 10,
  //       startPro: '01-01-2567',
  //       endPro: '31-12-2568',
  //       detail: 'ส่วนลดเพื่อ10บาท ในวันปี่ใหม่'
  //     }
  //   ]
  // }

  async function save() {
    await savePromotion()
    closeDialog()
  }

  async function savePromotion() {
    loadindStore.doLoad()
    const promotion = editedPromotion.value
    if (!promotion.id) {
      console.log('Post' + JSON.stringify(promotion))
      const res = await promotionService.addPromotion(promotion)
    } else {
      console.log('Patch' + JSON.stringify(promotion))
      const res = await promotionService.updatePromotion(promotion)
    }
    clearForm()
    await getPromotions()
    loadindStore.finish()
  }

  function closeHistoryDialog() {
    dialog.value = false
  }

  function closeDialog() {
    dialog.value = false
    clearForm()
  }

  function closeDelete() {
    dialogDelete.value = false
    clearForm()
  }

  async function deleteItemConfirm() {
    await deletePromotion()
    closeDelete()
  }

  async function deletePromotion() {
    loadindStore.doLoad()
    const promotion = editedPromotion.value
    const res = await promotionService.delPromotion(promotion)
    clearForm()
    await getPromotions()
    loadindStore.finish()
  }
  async function deleteItem(item: Promotion) {
    if (!item.id) return
    await getPromotion(item.id)
    dialogDelete.value = true
    editedIndex = -1
  }

  function showloginDialog() {
    dialog.value = true
  }

  function onSubmit() {}
  function close() {
    dialog.value = false
    nextTick(() => {
      editedPromotion.value = Object.assign({}, initialPromotion)
      editedIndex = -1
    })
  }

  async function editItem(item: Promotion) {
    if (!item.id) return
    if (!item.id) return
    await getPromotion(item.id)
    dialog.value = true
    editedIndex = -1
  }

  async function getPromotion(id: number) {
    loadindStore.doLoad()
    const res = await promotionService.getPromotion(id)
    editedPromotion.value = res.data
    loadindStore.finish()
  }

  function clearForm() {
    editedPromotion.value = JSON.parse(JSON.stringify(initialPromotion))
  }

  const selectedPromotions = ref<string[]>([])
  const promo = ref<Promotion | null>(null) // Use ref with a single promotion or null
  //const receiptStore = useReceiptStore() // Import and use the receipt store
  const discount = ref(0)

  function getProdiscount(selectedPromotions: string[]) {
    let discount = 0

    for (const selectedPromotion of selectedPromotions) {
      if (selectedPromotion === 'โปรวันเกิดลด 20 บาท') {
        discount += 20
      } else if (selectedPromotion === 'โปรโมชั่นวันปีใหม่ลด 10 บาท') {
        discount += 10
      }
    }

    // ลด totalBefore ตามค่า discount จาก receiptStore
    //receiptStore.receipt!.totalPrice -= discount

    return discount
  }

  function clear() {
    selectedPromotions.value = []
  }

  function resetSelectedPromotions() {
    selectedPromotions.value = [] // Assuming selectedPromotions is a ref or reactive variable
  }

  return {
    dialog,
    dialogDelete,
    from,
    loading,
    editedPromotion,
    promotions,
    promo,
    discount,
    showloginDialog,
    save,
    closeDialog,
    closeDelete,
    deleteItemConfirm,
    deleteItem,
    onSubmit,
    close,
    editItem,
    getProdiscount,
    clear,
    resetSelectedPromotions,
    getPromotions,
    getPromotion,
    clearForm,
    closeHistoryDialog
  }
}
