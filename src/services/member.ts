import type { Member } from '@/types/Member'
import http from './http'

function addMember(member: Member) {
  return http.post('/members', member)
}

function updateMember(member: Member) {
  return http.patch(`/members/${member.id}`, member)
}

function delMember(member: Member) {
  return http.delete(`/members/${member.id}`)
}

function getMember(id: number) {
  return http.get(`/members/${id}`)
}
function getMembers() {
  return http.get('/members')
}

function getMemberByEmail(email: string) {
  console.log(email)
  return http.get(`/members/email/${email}`)
}

export default { addMember, updateMember, delMember, getMember, getMembers, getMemberByEmail }
