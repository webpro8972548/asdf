import type { Product } from '@/types/Product'
import http from './http'
import category from './category'

function addProduct(product: Product & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', product.name)
  formData.append('price', JSON.stringify(product.price))
  formData.append('category', JSON.stringify(product.category))
  formData.append('subCategorys', JSON.stringify(product.subCategorys))
  formData.append('sizes', JSON.stringify(product.sizes))
  formData.append('file', product.files[0])
  return http.post('/products', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateProduct(product: Product & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', product.name)
  formData.append('price', JSON.stringify(product.price))
  formData.append('category', JSON.stringify(product.category))
  if (product.subCategorys && product.subCategorys.length > 0) {
    formData.append('subCategorys', JSON.stringify(product.subCategorys))
  } else {
    formData.append('subCategorys', JSON.stringify(product.subCategorys))
  }
  if (product.sizes && product.sizes.length > 0) {
    formData.append('sizes', JSON.stringify(product.sizes))
  } else {
    formData.append('sizes', JSON.stringify(product.sizes))
  }
  if (product.files && product.files.length > 0) {
    formData.append('file', product.files[0])
  }
  return http.post(`/products/${product.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

// function getProductsByCategory(categoryId: number) {
//   return http.get('/products/categorys/' + categoryId)
// }
function getProductsByCategory(categoryId: number) {
  return http.get(`/products/category/${categoryId}`)
}

function delProduct(product: Product) {
  return http.delete(`/products/${product.id}`)
}
function getProduct(id: number) {
  return http.get(`/products/${id}`)
}
function getProducts() {
  return http.get('/products')
}

function getByFilter(categoryId: number) {
  console.log('categoryId : ' + categoryId)
  return http.get(`/users/filter/${categoryId}`)
}

export default {
  addProduct,
  updateProduct,
  delProduct,
  getProduct,
  getProducts,
  getProductsByCategory,
  getByFilter
}
