import type { User } from '@/types/User'
import http from './http'

function addUser(user: User & { files: File[] }) {
  const formData = new FormData()
  //Informations
  formData.append('name', user.name)
  formData.append('gender', user.gender)
  formData.append('height', user.height.toString())
  formData.append('weight', user.weight.toString())
  formData.append('bloodType', user.bloodType)
  formData.append('age', user.age.toString())
  formData.append('birthDate', user.birthDate.toString())
  //contact
  formData.append('phone', user.phone)
  formData.append('email', user.email)
  formData.append('address', user.address)
  //work
  formData.append('role', JSON.stringify(user.role))
  formData.append('startDate', user.startDate.toString())
  formData.append('status', user.status)
  formData.append('salary', user.salary.toString())
  formData.append('branch', JSON.stringify(user.branch))
  //login
  formData.append('password', user.password)
  //image
  if (user.files && user.files.length > 0) {
    formData.append('file', user.files[0])
  }
  return http.post('/users', formData, { headers: { 'Content-Type': 'multipart/form-data' } })
}

function updateUser(user: User & { files: File[] }) {
  const formData = new FormData()
  //Informations
  formData.append('name', user.name)
  formData.append('gender', user.gender)
  formData.append('height', user.height.toString())
  formData.append('weight', user.weight.toString())
  formData.append('bloodType', user.bloodType)
  formData.append('age', user.age.toString())
  formData.append('birthDate', user.birthDate.toString())
  //contact
  formData.append('phone', user.phone)
  formData.append('email', user.email)
  formData.append('address', user.address)
  //work
  formData.append('role', JSON.stringify(user.role))
  formData.append('startDate', user.startDate.toString())
  formData.append('status', user.status)
  formData.append('salary', user.salary.toString())
  formData.append('branch', JSON.stringify(user.branch))
  //login
  formData.append('password', user.password)
  //image
  if (user.files && user.files.length > 0) {
    formData.append('file', user.files[0])
  }
  return http.patch(`/users/${user.id}`, formData, {
    headers: { 'Content-Type': 'multipart/form-data' }
  })
}

async function changePassword(user: User) {
  console.log(user)
  return await http.patch('users/changepassword', { email: user.email, password: user.password })
}
function delUser(user: User) {
  return http.delete(`/users/${user.id}`)
}
function getUser(id: number) {
  return http.get(`/users/${id}`)
}
function getUsers() {
  return http.get('/users')
}

function getUsersByBranch(branchId: number) {
  return http.get(`/users/branch/${branchId}`)
}

function getUserByEmail(email: string) {
  console.log(email)
  return http.get(`/users/email/${email}`)
}

function getByFilter(gender: string, role: string, branch: string, status: string, minSalary: number, maxSalary: number, minAge: number, maxAge: number) {
  console.log("gender: " + gender + " role: " + role + "Branch : " + branch + "status : " + status + "minSalary: " + minSalary + " maxSalary: " + maxSalary + " minAge: " + minAge + " maxAge: " + maxAge)
  return http.get(`/users/filter/${role}/${gender}/${branch}/${status}/${minSalary}/${maxSalary}/${minAge}/${maxAge}`)
}

function CalculateSalary(id: number) {
  return http.post(`/users/users/${id}`)
}
export default {
  addUser,
  updateUser,
  delUser,
  getUser,
  getUsers,
  getUserByEmail,
  getUsersByBranch,
  changePassword,
  getByFilter,
  CalculateSalary
}
